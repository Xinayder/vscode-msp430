'use strict';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as ChildProcess from 'child_process';
import * as fs from 'fs';
import * as path from 'path';

let taskProvider: vscode.Disposable | undefined;

// Generate tasks to be used with the MSP430 MCUs
function getMSPTasks(): vscode.Task[] {
    let tasks: vscode.Task[] = [];
    let compileArgs: string[] = ["${config:msp430.compiler.path}"];
    let uploadArgs: string[] = ["${config:msp430.debugger.path}"];

    let config = vscode.workspace.getConfiguration("msp430");

    // msp430-elf-gcc requires both -I and -L (include files and library files, respectively)
    // pointing to the same location
    if (config.get("compiler.useCustomIncludePath")) {
        compileArgs.push("-I ${config:msp430.compiler.includePath}", "-L ${config:msp430.compiler.includePath}");
    }
    compileArgs.push("-O${config:msp430.compiler.optimizationLevel}", "-mmcu=${config:msp430.compiler.target}", "${file}", "-o ${fileBasenameNoExtension}.out");

    uploadArgs.push("${config:msp430.debugger.driver}", "'prog ${fileBasenameNoExtension}.out'");

    tasks.push(new vscode.Task({type: "msp430", action: "compile"}, "compile", "msp430", new vscode.ShellExecution(compileArgs.join(" "))));
    tasks.push(new vscode.Task({type: "msp430", action: "upload"}, "upload", "msp430", new vscode.ShellExecution(uploadArgs.join(" "))));

    return tasks;
}

// Called when the extension is activated
export function activate(context: vscode.ExtensionContext) {
    let timeout: NodeJS.Timer | null = null;

    let activeEditor = vscode.window.activeTextEditor;
    if (activeEditor) {
        triggerUpdateMmcu();
    }

    // Register to active textEditor changes
    vscode.window.onDidChangeActiveTextEditor(editor => {
        activeEditor = editor;
        if (editor) {
            triggerUpdateMmcu();
        }
    }, null, context.subscriptions);

    // Register to text document changes
    vscode.workspace.onDidChangeTextDocument(event => {
        if (activeEditor && event.document === activeEditor.document) {
            triggerUpdateMmcu();
        }
    }, null, context.subscriptions);

    // Updates the timer to update the mmcu.
    function triggerUpdateMmcu() {
        if (timeout) {
            clearTimeout(timeout);
        }
        timeout = setTimeout(updateMmcu, 100);
    }

    // Update the target mmcu according to the currently open file
    function updateMmcu() {
        if (!activeEditor) {
            return;
        }

        const regex = /^(?:#include\s<(msp430[0-9a-z]+).h>)/g;
        const text = activeEditor.document.getText();
        let match;
        while (match = regex.exec(text)) {
            vscode.workspace.getConfiguration("msp430").update("compiler.target", match[1], vscode.ConfigurationTarget.Workspace);
        }
    }

    taskProvider = vscode.tasks.registerTaskProvider('msp430', {
        provideTasks: () => {
            let mspTasks = getMSPTasks();
            return mspTasks;
        },
        resolveTask(_task: vscode.Task): vscode.Task | undefined {
            return undefined;
        }
    });

    // Check if we started one of our defined tasks
    vscode.tasks.onDidStartTask((e: vscode.TaskStartEvent) => {
        let config = vscode.workspace.getConfiguration("msp430");
        let taskName = e.execution.task.name.toLowerCase();
        let error = false;
        let message = "";

        switch (taskName) {
            case "compile":
                if (!config.get("compiler.target")) {
                    error = true;
                    message = "Please select a microcontroller.";
                } else if (!config.get("compiler.path")) {
                    error = true;
                    message = "Please specify the path to the msp430-gcc executable.";
                } else if (!config.get("compiler.optimizationLevel")) {
                    error = true;
                    message = "Please choose an optimization level.";
                }
                break;
            case "upload":
                if (!config.get("debugger.path")) {
                    error = true;
                    message = "Please specify the path to the mspdebug executable.";
                } else if (!config.get("debugger.driver")) {
                    error = true;
                    message = "Please specify a driver to be used with mspdebug.";
                }
                break;
        }

        // If there's an error, show it, with a button to configure the extension.
        if (error) {
            e.execution.terminate();
            vscode.window.showErrorMessage(message, "Configure").then(() => {
                vscode.commands.executeCommand('workbench.action.openSettings');
            });
        }

    });

    let btnCompile = vscode.commands.registerTextEditorCommand("msp430.compile", () => {
        vscode.commands.executeCommand("workbench.action.tasks.runTask", "msp430: compile");
    });

    let btnUpload = vscode.commands.registerTextEditorCommand("msp430.upload", () => {
        vscode.commands.executeCommand("workbench.action.tasks.runTask", "msp430: upload");
    });

    context.subscriptions.push(btnCompile, btnUpload);
}

// Called when the extension is deactivated
export function deactivate() {
    if (taskProvider) {
        taskProvider.dispose();
    }
}